package com.apolexian.msg.authentication

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.apolexian.msg.message.AllMessagesActivity
import com.apolexian.msg.R
import com.apolexian.msg.models.User
import com.apolexian.msg.models.fields.Email
import com.apolexian.msg.models.fields.Password
import com.apolexian.msg.models.fields.Username
import com.apolexian.msg.utils.BitmapResolver
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
    var selectedPhotoUri: Uri? = null
    private val TAG = "RegisterActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn_registration_register.setOnClickListener {
            val email = Email(et_registration_email.text.toString())
            val password = Password(et_registration_password.text.toString())
            if (!email.valid() || !password.valid()) {
                Toast
                    .makeText(this, "Invalid values provided", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            }
            performRegistration(email, password)
        }

        btn_registration_select_photo.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        tv_registration_redirect_sign_in.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    // uses the circle image view library to make the photo look nice and fit on selection
    // https://github.com/hdodenhof/CircleImageView
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d(TAG, "Photo was selected")
            selectedPhotoUri = data.data
            val bitmap = BitmapResolver.getBitmap(this.contentResolver, selectedPhotoUri)
            iv_registration_circle_image_view.setImageBitmap(bitmap)
            // 0f so that the button does not overlap the circle image view
            btn_registration_select_photo.alpha = 0f
        }
    }

    private fun performRegistration(email: Email, password: Password) {
        if (email.getAddress().isEmpty() || password.getValue().isEmpty()) {
            Toast
                .makeText(this, "Fields can not be empty", Toast.LENGTH_LONG)
                .show()
            return
        }
        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(email.getAddress(), password.getValue())
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener
                Log.d(
                    TAG,
                    "Successfully creates user with uid: ${it.result!!.user!!.uid}"
                )
                uploadSelectedPhotoToFirebaseStorage()
            }.addOnFailureListener {
                Log.d(TAG, "Failed to create user with error: ${it.message}")
                Toast.makeText(
                    this,
                    "Failed to create user with error: ${it.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    private fun uploadSelectedPhotoToFirebaseStorage() {
        if (selectedPhotoUri == null) return
        val filename = UUID.randomUUID().toString()
        val uploadLocationReference =
            FirebaseStorage.getInstance().getReference("/images/$filename")
        uploadLocationReference.putFile(selectedPhotoUri!!).addOnSuccessListener { it ->
            Log.d(TAG, "Uploaded image: ${it.metadata?.path}")
            uploadLocationReference.downloadUrl.addOnSuccessListener {
                it.toString()
                Log.d(TAG, "File Location: $it")
                saveUserInformationToFirebase(it.toString())
            }
        }.addOnFailureListener {
            Log.d(
                TAG, "Failed to upload image to storage" +
                        "with status: $it"
            )
        }
    }

    // saves user to real time database, saving handles by user to abstract
    private fun saveUserInformationToFirebase(profileImageUrl: String) {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val userDatabaseReference =
            FirebaseDatabase.getInstance().getReference("/users/$currentUserUid")
        val username = Username(et_registration_username.text.toString())
        if (username.valid() && currentUserUid != null) {
            val user = User(currentUserUid, username.getValue(), profileImageUrl)
            user.saveToFirebase(userDatabaseReference)
            val intent = Intent(this, AllMessagesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            Toast
                .makeText(this, "Invalid username provided.", Toast.LENGTH_LONG)
                .show()
            return
        }
    }
}
