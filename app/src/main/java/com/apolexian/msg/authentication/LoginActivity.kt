package com.apolexian.msg.authentication

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.apolexian.msg.R
import com.apolexian.msg.models.fields.Email
import com.apolexian.msg.models.fields.Password
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login_login.setOnClickListener {
            val email = Email(et_login_email.text.toString())
            val password = Password(et_login_password.text.toString())
            if (!email.valid() || !password.valid()) {
                Toast
                    .makeText(this, "Invalid values provided", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            }
            performLogin(email, password)
        }

        tv_login_redirect_registration.setOnClickListener {
            finish()
        }
    }

    private fun performLogin(email: Email, password: Password) {
        if (email.getAddress().isEmpty() || password.getValue().isEmpty()) {
            Toast
                .makeText(this, "Fields can not be empty", Toast.LENGTH_LONG)
                .show()
            return
        }

        FirebaseAuth.getInstance()
            .signInWithEmailAndPassword(email.getAddress(), password.getValue())
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener
                Log.d(
                    "Main",
                    "Successfully logged user in: ${it.result?.user?.uid}"
                )
            }.addOnFailureListener {
                Log.d("Main", "Failed to log user in with error: ${it.message}")
            }
    }
}