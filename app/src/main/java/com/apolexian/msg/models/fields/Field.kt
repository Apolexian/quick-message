package com.apolexian.msg.models.fields

// Field represents a form field for the user object. This can be for example email.
interface Field {
    // validator for field, can be used to check if email address is valid or if password
    // contains all needed characters
    fun valid(): Boolean
    override fun toString(): String
}