package com.apolexian.msg.models

import android.os.Parcelable
import android.util.Log
import com.google.firebase.database.DatabaseReference
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    private val uid: String? = "",
    private val username: String? = "",
    private val profileImageUrl: String? = ""
) : Parcelable {

    fun saveToFirebase(databaseReference: DatabaseReference) {
        databaseReference.setValue(this).addOnSuccessListener {
            Log.d("Database Saving", "Saved user to real time database")
        }.addOnFailureListener {
            Log.d(
                "Database Saving", "Failed to add user to real time database " +
                        "with status: $it"
            )
        }
    }

    fun getUsername(): String {
        return this.username ?: ""
    }

    fun getProfileImageUri(): String {
        return this.profileImageUrl ?: ""
    }

    fun getUserUid(): String {
        return this.uid ?: ""
    }
}