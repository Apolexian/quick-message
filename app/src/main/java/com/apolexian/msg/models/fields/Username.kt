package com.apolexian.msg.models.fields

import java.util.regex.Pattern

class Username(private val value: String) : Field {
    override fun valid(): Boolean {
        return Pattern.compile(
            "([A-Z]|[a-z])([A-Z]|[a-z]|[0-9]){2,8}"
        ).matcher(this.value).matches()
    }

    override fun toString(): String {
        return this.value
    }

    fun getValue(): String {
        return this.value
    }
}