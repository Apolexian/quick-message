package com.apolexian.msg.models.fields

import java.util.regex.Pattern

class Password(private val value: String) : Field {
    override fun valid(): Boolean {
        return Pattern.compile(
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[^da-zA-Z]).{5,}$"
        ).matcher(this.value).matches()
    }

    override fun toString(): String {
        return this.value
    }

    fun getValue(): String {
        return this.value
    }
}