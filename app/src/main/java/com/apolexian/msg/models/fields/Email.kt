package com.apolexian.msg.models.fields

import java.lang.IllegalArgumentException
import java.util.regex.Pattern

class Email(private val address: String) : Field {

    override fun valid(): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(this.address).matches()
    }

    override fun toString(): String {
        return this.address
    }

    fun getAddress(): String {
        return this.address
    }

    fun getProvider(): String {
        if (!this.valid()) throw IllegalArgumentException("Invalid email address supplied.")
        return this.address.substring(
            this.address.indexOf("@"),
            this.address.indexOf(".")
        )
    }
}