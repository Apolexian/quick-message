package com.apolexian.msg.models

class Message(
    val id: String,
    val fromUserUId: String,
    val toUserUid: String,
    val textMessage: String,
    val timestamp: Long
) {
    constructor() : this("", "", "", "", -1)

    fun getMessageId(): String {
        return this.id
    }
}
