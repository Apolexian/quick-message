package com.apolexian.msg.message

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.apolexian.msg.R
import com.apolexian.msg.authentication.RegisterActivity
import com.apolexian.msg.models.Message
import com.apolexian.msg.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_all_messages.*

class AllMessagesActivity : AppCompatActivity() {

    companion object {
        val TAG = "All Messages Activity"
        var currentAuthenticatedUser: User? = null
        val allMessagesAdapter = GroupAdapter<GroupieViewHolder>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_messages)
        rv_all_messages.adapter = allMessagesAdapter
        fetchCurrentUser()
        redirectIfUserNotAuthenticated()
        listenForLatestMessages()
    }

    private fun listenForLatestMessages() {
        val userFromId = FirebaseAuth.getInstance().uid
        val latestMessagesDatabaseReference = FirebaseDatabase.getInstance()
            .getReference("/latest-messages/$userFromId")
        latestMessagesDatabaseReference.addChildEventListener(object : ChildEventListener {
            val latestMessagesMap = HashMap<String, Message>()
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                trackAndUpdateLatestMessages(p0)
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                trackAndUpdateLatestMessages(p0)
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                TODO("Not yet implemented")
            }

            private fun trackAndUpdateLatestMessages(p0: DataSnapshot) {
                val chatMessage = p0.getValue(Message::class.java) ?: return
                latestMessagesMap[p0.key!!] = chatMessage
                refreshAllMessagesRecyclerView()
            }

            private fun refreshAllMessagesRecyclerView() {
                allMessagesAdapter.clear()
                latestMessagesMap.values.forEach {
                    allMessagesAdapter.add(LatestMessageItem(it))
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.navigation_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_new_message -> {
                redirectToNewMessageActivity()
            }
            R.id.menu_log_out -> {
                menuOptionLogOut()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun menuOptionLogOut() {
        FirebaseAuth.getInstance().signOut()
        redirectIfUserNotAuthenticated()
    }

    private fun redirectIfUserNotAuthenticated() {
        if (!validateAuthenticatedStatus()) {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun validateAuthenticatedStatus(): Boolean {
        val currentUserUid = FirebaseAuth.getInstance().uid
        return currentUserUid != null
    }

    private fun redirectToNewMessageActivity() {
        val intent = Intent(this, NewMessageActivity::class.java)
        startActivity(intent)
    }

    private fun fetchCurrentUser() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            Log.d(TAG, "Failed to fetch currently authenticated user")
        }
        val currentUserDatabaseReference = FirebaseDatabase
            .getInstance()
            .getReference("users/$uid")

        currentUserDatabaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {

                currentAuthenticatedUser = User(
                    p0.key,
                    p0.child("username").getValue(String::class.java),
                    p0.child("profileImageUri").getValue(String::class.java)
                )

                if (currentAuthenticatedUser != null) {
                    Log.d(
                        TAG,
                        "Updated currently authenticated user to: " +
                                currentAuthenticatedUser!!.getUserUid()
                    )
                } else {
                    Log.d(TAG, "Failed to update currently authenticated user")
                }
            }
        })
    }
}
