package com.apolexian.msg.message

import com.apolexian.msg.R
import com.apolexian.msg.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.row_chat_left.view.*

class ChatItemLeft(private val textMessage: String, val user: User) : Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.row_chat_left
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.tv_row_chat_left_msg.text = this.textMessage
        Picasso.get()
            .load(user.getProfileImageUri())
            .into(viewHolder.itemView.iv_row_chat_left_circle_image_view)
    }
}