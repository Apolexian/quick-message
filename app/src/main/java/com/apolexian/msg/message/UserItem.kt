package com.apolexian.msg.message

import com.apolexian.msg.R
import com.apolexian.msg.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.row_user.view.*

class UserItem(val user: User) : Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.row_user
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.tv_row_user_username.text = user.getUsername()

        Picasso.get().load(user.getProfileImageUri())
            .into(viewHolder.itemView.iv_row_chat_right_circle_image_view)
    }
}