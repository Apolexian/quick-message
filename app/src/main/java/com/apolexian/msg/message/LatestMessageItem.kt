package com.apolexian.msg.message

import com.apolexian.msg.R
import com.apolexian.msg.models.Message
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.row_latest_messages.view.*

class LatestMessageItem(val message: Message) : Item<GroupieViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.row_latest_messages
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.tv_latest_row_latest_messages.text = message.textMessage
    }

}