package com.apolexian.msg.message

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.apolexian.msg.R
import com.apolexian.msg.models.Message
import com.apolexian.msg.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity() {
    private val TAG = "Chat Activity"
    val chatAdapter = GroupAdapter<GroupieViewHolder>()
    var toUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        toUser = intent.getParcelableExtra(NewMessageActivity.USER_KEY)
        if (toUser != null) {
            supportActionBar?.title = toUser!!.getUsername()
        } else {
            supportActionBar?.title = "Chat"
            Log.d(this.TAG, "Could not parse username from intent")
        }
        listenForNewMessages()
        btn_send_chat.setOnClickListener {
            Log.d(this.TAG, "Attempt to send message")
            sendMessage()
        }
        rv_chat.adapter = chatAdapter
    }

    private fun sendMessage() {
        val userFromUid = FirebaseAuth.getInstance().uid
        if (userFromUid != null && toUser != null) {
            val toUserUid = toUser!!.getUserUid()
            val fromUserMessageReference = pushUserMessageDatabaseReference(userFromUid, toUserUid)
            val toUserMessageReference = pushUserMessageDatabaseReference(toUserUid, userFromUid)

            val textMessage = Message(
                fromUserMessageReference.key!!,
                FirebaseAuth.getInstance().uid!!,
                toUserUid,
                et_chat_message_text.text.toString(),
                System.currentTimeMillis() / 1000
            )
            updateUserMessages(
                fromUserMessageReference,
                textMessage,
                toUserMessageReference
            )
            updateLatestMessages(
                userFromUid,
                toUserUid,
                textMessage
            )
            clearTextViewAndScrollDownAfterSending()
        } else {
            Log.d(
                TAG,
                "Could not validate either user: " +
                        "\n From user: $userFromUid " +
                        "\n To User: $toUser"
            )
            return
        }
    }

    private fun updateUserMessages(
        fromUserMessageReference: DatabaseReference,
        textMessage: Message,
        toUserMessageReference: DatabaseReference
    ) {
        putNewMessageIntoDatabase(
            fromUserMessageReference,
            textMessage,
            toUserMessageReference
        )
        putNewMessageIntoDatabase(
            toUserMessageReference,
            textMessage,
            fromUserMessageReference
        )
    }

    private fun pushUserMessageDatabaseReference(
        userOneId: String?,
        userTwoId: String?
    ): DatabaseReference {
        return FirebaseDatabase
            .getInstance()
            .getReference("/user-messages/$userOneId/$userTwoId")
            .push()
    }

    private fun putNewMessageIntoDatabase(
        userMessageReference: DatabaseReference,
        textMessage: Message,
        messagesReference: DatabaseReference
    ) {
        userMessageReference.setValue(textMessage).addOnSuccessListener {
            Log.d(
                this.TAG,
                "Pushed message object do database with id: $messagesReference.key"
            )
        }
    }

    private fun updateLatestMessages(
        userOneId: String?,
        userTwoId: String?,
        textMessage: Message
    ) {
        val latestMessageDatabaseReference = FirebaseDatabase.getInstance()
            .getReference("/latest-messages/$userOneId/$userTwoId")
        latestMessageDatabaseReference.setValue(textMessage)
    }

    private fun clearTextViewAndScrollDownAfterSending() {
        et_chat_message_text.setText("")
        rv_chat.scrollToPosition(chatAdapter.itemCount - 1)
    }

    private fun listenForNewMessages() {
        val toUserUid = toUser!!.getUserUid()
        val userFromId = FirebaseAuth.getInstance().uid
        val messagesReference =
            FirebaseDatabase.getInstance().getReference("/user-messages/$userFromId/$toUserUid")
        messagesReference.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val chatMessage = p0.getValue(Message::class.java)

                if (chatMessage != null) {
                    Log.d(TAG, "Retrieved message: ${chatMessage.getMessageId()}")
                    if (toUser == null) {
                        Log.d(TAG, "Could not retrieve to user")
                        return
                    }
                    if (chatMessage.fromUserUId == FirebaseAuth.getInstance().uid) {
                        val currentAuthenticatedUser =
                            AllMessagesActivity.currentAuthenticatedUser ?: return
                        chatAdapter.add(
                            ChatItemLeft(
                                chatMessage.textMessage,
                                currentAuthenticatedUser
                            )
                        )
                    } else {
                        chatAdapter.add(ChatItemRight(chatMessage.textMessage, toUser!!))
                    }
                } else {
                    Log.d(TAG, "Failed to load message")
                    return
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {}
        })
    }
}