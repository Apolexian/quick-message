package com.apolexian.msg.message

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.apolexian.msg.R
import com.apolexian.msg.models.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_new_message.*

class NewMessageActivity : AppCompatActivity() {
    val TAG = "New Message Activity"

    companion object {
        val USER_KEY = "USER_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)
        supportActionBar?.title = "Select User"
        fetchUsersFromFirebase()
    }

    private fun fetchUsersFromFirebase() {
        val userDatabaseRef = FirebaseDatabase
            .getInstance()
            .getReference("/users")

        userDatabaseRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(p0: DataSnapshot) {
                val adapter = GroupAdapter<GroupieViewHolder>()
                p0.children.forEach {
                    Log.d(TAG, "Fetching user: $it")
                    val user = User(
                        it.key,
                        it.child("username").getValue(String::class.java),
                        it.child("profileImageUri").getValue(String::class.java)
                    )
                    adapter.add(UserItem(user))
                }
                adapter.setOnItemClickListener { item, view ->
                    item as UserItem
                    val intent = Intent(view.context, ChatActivity::class.java)
                    intent.putExtra(USER_KEY, item.user)
                    startActivity(intent)
                    finish()
                }
                view_new_message_recycler_view.adapter = adapter
            }

        })

    }
}
