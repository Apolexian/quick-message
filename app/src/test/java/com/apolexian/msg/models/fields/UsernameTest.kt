package com.apolexian.msg.models.fields

import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class UsernameTest {
    @Test
    @DisplayName("Username with only numbers is invalid")
    fun onlyNumbers() {
        val testUsername = Username("1234")
        testUsername.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Username can not exceed 9 characters")
    fun overNineChars() {
        val testUsername = Username("overNineCharacters")
        testUsername.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Username can not be under 3 characters")
    fun underThreeChars() {
        val testUsername = Username("ab")
        testUsername.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Username should not contain _")
    fun underscore() {
        val testUsername = Username("under_score")
        testUsername.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Valid username between 3 to 9 characters, contains letters and numbers")
    fun valid() {
        val testUsername = Username("abcd1234")
        testUsername.valid() `should be equal to` true
    }
}