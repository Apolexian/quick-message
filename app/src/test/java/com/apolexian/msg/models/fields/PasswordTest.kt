package com.apolexian.msg.models.fields

import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class PasswordTest {

    @Test
    @DisplayName("Password with valid format should be validated.")
    fun valid() {
        val testPassword = Password("abcd1234ABCD!")
        testPassword.valid() `should be equal to` true
    }

    @Test
    @DisplayName("Password with no letters should be invalid.")
    fun invalidNoLetters() {
        val testPass = Password("12341234!")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password with no numbers should be invalid.")
    fun invalidNoNumbers() {
        val testPass = Password("abcdabcd!")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password with no symbols should be invalid")
    fun invalidNoSymbol() {
        val testPass = Password("abcd1234")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password with only letters should be invalid")
    fun invalidOnlyLetters() {
        val testPass = Password("abcdabcd")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password with only numbers should be invalid")
    fun invalidOnlyNumbers() {
        val testPass = Password("12341234")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password with only symbols should be invalid")
    fun invalidOnlySymbols() {
        val testPass = Password("!!!!!!!!")
        testPass.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Password of length less than 5 should not be valid")
    fun invalidLessThanTenCharacters() {
        val testPass = Password("abcd")
        testPass.valid() `should be equal to` false
    }
}