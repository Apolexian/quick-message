package com.apolexian.msg.models.fields

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should throw`
import org.amshove.kluent.`with message`
import org.amshove.kluent.invoking
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class EmailTest {

    @Test
    @DisplayName("Invalid email address is not validated")
    fun notValid() {
        val testEmail = Email("invalid_email")
        testEmail.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Address with just @ symbol should not be valid")
    fun onlyAtSymbol() {
        val testEmail = Email("@")
        testEmail.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Address with just . symbol should not be valid")
    fun onlyDotSymbol() {
        val testEmail = Email(".")
        testEmail.valid() `should be equal to` false
    }

    @Test
    @DisplayName("Valid email address is validated")
    fun valid() {
        val testEmail = Email("test@gmail.com")
        testEmail.valid() `should be equal to` true
    }

    @Test
    @DisplayName("Provider of a gmail address should be gmail")
    fun testProvider() {
        val testEmail = Email("test@gmail.com")
        testEmail.getProvider() `should be equal to` "@gmail"
    }

    @Test
    @DisplayName("Invalid email address should not return a provider")
    fun testInvalidEmailProvider() {
        val testEmail = Email("invalid")
        invoking {
            testEmail.getProvider()
        } `should throw` IllegalArgumentException::class `with message` "Invalid email address supplied."
    }
}