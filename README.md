# Quick message

Quick message is a messaging app built in Kotlin. It was built for hobby and education purposes, so
it won't be as feature full as other available messaging apps.

## Getting Started

Clone the repo:
```bash
workspace: git clone https://github.com/Apolexian/Quick-Message.git
```

### Installing

TODO

## Running the tests

TODO

### And coding style tests


## Built With
* Kotlin - https://kotlinlang.org/

## Contributing

TODO

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
